import { Box, IconButton, Grid } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import IDrawer, { IContent } from './types/IDrawers';
import { useOpenDrawerContent } from '../hooks/useOpenDrawerContent';
import { SubitemDrawerContent } from './SubitemDrawerContent';
import { DrawerContentWithoutSubitem } from './DrawerContentWithoutSubitem';
import React from 'react';

export const DrawerContent: React.FC<IDrawer> = ({ title, content, drawerContentStyle, dataTestid }: IDrawer) => {
    
    const theme = useTheme();
    const {closeTheDrawer} = useOpenDrawerContent(); 
    return (
        <div className={drawerContentStyle}>
            <Grid container >
                <Grid item xs={8} data-testid='title'>
                    {title} 
                </Grid>
                <Grid item xs={2}>
                    <IconButton 
                        onClick={closeTheDrawer} 
                        id='buttonIconArrowClosed'
                        data-testid={ `buttonIconArrowClosed-${dataTestid}` }                      
                    >
                        {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
                    </IconButton>
                </Grid>
            </Grid>
            <Box> 
            {
                content.map((content: IContent, indexContent: number ) => {  
                    const {subItemTitleDrawer} = content;
                    return (                       
                        subItemTitleDrawer?.length ?
                            (   
                            <SubitemDrawerContent
                                key={indexContent} 
                                content={ [content] }                      
                            />                
                            ):
                            <DrawerContentWithoutSubitem 
                                key={indexContent}                           
                                content={ [content] }
                            />                    
                    )                      
                })  
            }
            </Box>
        </div>
    );
};