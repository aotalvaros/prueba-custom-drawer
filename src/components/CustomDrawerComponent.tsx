import { Box, CssBaseline, Drawer, IconButton } from '@mui/material'
import { RootStateOrAny, useSelector } from 'react-redux';
import { DrawerContent } from './DrawerContent'
import  IDrawer  from './types/IDrawers';
import MenuIcon from '@mui/icons-material/Menu';
import { useOpenDrawerContent } from '../hooks/useOpenDrawerContent';
import React from 'react';

import '../styles/styles.scss'

export const CustomDrawerComponent: React.FC<IDrawer> = ({ title, content, drawerStyle, drawerContentStyle, showIconButtonMenu }: IDrawer) => {

    const {opendTheDrawer} = useOpenDrawerContent();
    const {active} = useSelector((state: RootStateOrAny) => state.ui);

    return (
        <Box
            sx={{
                width: "50px"
            }}
        >
            <CssBaseline />
            { 
                showIconButtonMenu &&
                    <IconButton
                        className='style_icon_button_menu_open_drawer'
                        data-testid='icon-button-menu-open-drawer' 
                        disabled={active}
                        edge="start"           
                        onClick={opendTheDrawer}
                    >
                        <MenuIcon/>
                    </IconButton> 
            } 

            <Drawer
                className={drawerStyle}  
                data-testid="custom-drawer-expansible"
                ModalProps={{ keepMounted: true }}
                open={active}
                variant="temporary"
                sx={{
                    display: { xs: 'block', sm: 'none' }
                }}
            >
                <DrawerContent
                    drawerContentStyle={drawerContentStyle}
                    content={content}
                    dataTestid="drawerContent-expansible"
                    title={title}
                /> 
            </Drawer> 

            <Drawer
                className={drawerStyle}   
                data-testid="custom-drawer-permanent"
                open={active}             
                variant="persistent"      
            >
                <DrawerContent
                    drawerContentStyle={drawerContentStyle}
                    content={content}
                    dataTestid="drawerContent-permanent"                  
                    title={title}
                />    
            </Drawer>           
        </Box>
    );
};