import { StyledEngineProvider } from '@mui/material';
import { Provider } from 'react-redux';
import IDrawer from './types/IDrawers';
import { store } from '../store/store';
import { CustomDrawerComponent } from './CustomDrawerComponent';
import React from 'react';

export const GobantDrawerComponent = ({title, content, showIconButtonMenu, drawerStyle, drawerContentStyle}:IDrawer) => { 
  return (
    <StyledEngineProvider injectFirst>
      <Provider store={store}>
        <CustomDrawerComponent
          title={title}
          content={content}      
          showIconButtonMenu={showIconButtonMenu}
          drawerStyle={drawerStyle}
          drawerContentStyle={drawerContentStyle}
        />
      </Provider>
    </StyledEngineProvider>    
  );
};
