import { Divider, ListItemButton, ListItemText } from '@mui/material'
import { NavLink } from 'react-router-dom'
import IDrawer from './types/IDrawers'
import React from 'react';
import { useOpenDrawerContent } from '../hooks/useOpenDrawerContent';

export const DrawerContentWithoutSubitem = ({content}:IDrawer) => {
    const { closeTheDrawer } = useOpenDrawerContent(); 

    return (
        <> 
            {
                content?.map(({itemTitleDrawer, path}, index) => {
                    return(
                        <div key={index}>
                            <ListItemButton                                
                                id='component_drawer_without_subitem'
                                data-testid={`component_drawer_without_subitem_${index}`}
                                component={NavLink}
                                to={path}onClick={() => closeTheDrawer()} 
                            >
                                <ListItemText 
                                    primary={itemTitleDrawer}                                  
                                />
                            </ListItemButton>
                            <Divider/>                 
                        </div>
                    )
                })
            }                                                                          
        </> 
    )
}
