import { Divider, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import { NavLink } from 'react-router-dom';
import LocalPoliceIcon from '@mui/icons-material/LocalPolice';
import { useOpenDrawerContent } from '../hooks/useOpenDrawerContent';
import { ExpandLess, ExpandMore } from '@mui/icons-material'; 
import IDrawer from './types/IDrawers';
import React from 'react';

export const SubitemDrawerContent = ({content}: IDrawer ) => {
    const [ { itemTitleDrawer, subItemTitleDrawer } ] = content;
    const { isOpenCustomDrawerContent, openAndCloseCustomDrawerContent, closeTheDrawer } = useOpenDrawerContent(); 

    return (
        <div>          
            <ListItemButton
                alignItems="flex-start"
                id='custom_drawer_content'           
                onClick={()=> openAndCloseCustomDrawerContent(itemTitleDrawer)}
                data-testid="custom_drawer_content_subitem"
            >
                <ListItemText
                    className={`${isOpenCustomDrawerContent[itemTitleDrawer] ? 'subitem_text_overflow_ellipsis' : 'drawer_element_arrow_downUp'}`}     
                    primary={itemTitleDrawer}                                
                    secondary={ `${subItemTitleDrawer?.map((name) => { return ` ${name.name}`})}`}
                />
                {
                    isOpenCustomDrawerContent[itemTitleDrawer] ? (<ExpandLess />) : (<ExpandMore />)
                }                                   
            </ListItemButton>
            {
                isOpenCustomDrawerContent[itemTitleDrawer] &&
                    subItemTitleDrawer?.map(({ name, path }, index: number) =>{                     
                        return(
                        <ListItemButton
                            key={index}
                            id='subitems_drawer'
                            data-testid={`subitems_drawer_${index}`}
                            component={NavLink}
                            to={path}
                            onClick={() => closeTheDrawer()}                                                                                                                         
                        >
                            <ListItemIcon key={index} data-testid="subitems_icon">
                                <LocalPoliceIcon/>
                            </ListItemIcon>
                            <ListItemText
                                data-testid="subitems_name"  
                                placeholder={`${index}`}                                             
                                primary={name}                             
                            />
                        </ListItemButton> 
                    )})
            }
            <Divider/>                                                 
        </div>
    )
};
