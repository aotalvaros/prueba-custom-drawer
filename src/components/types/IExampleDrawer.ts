
export default interface IExampleDRawer { 
    title: string, 
    content: IContent[], 
}

interface IContent {   
    dataTestid?: string
    indexContent?: number;
    itemTitleDrawer: string;
    path: string,
    subItemTitleDrawer: IRoutes[];
};

interface IRoutes {
    name: string
    path: string,
}