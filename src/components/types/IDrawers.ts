
export default interface IDrawer {
    content: IContent[], 
    dataTestid?: string,
    drawerContentStyle?: string,
    drawerStyle?: string,
    showIconButtonMenu?: boolean
    title?: string, 
};

export interface IContent {   
    indexContent?: number;
    itemTitleDrawer: string;
    path: string,
    subItemTitleDrawer?: IRoutes[] ;
};

interface IRoutes {
    name: string
    path: string,
};
