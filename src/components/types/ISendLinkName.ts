
export interface ISendLinkName {
    titleDrawer: string,
    url: string | null,
    subItemDrawer? : string
}