import { useState } from 'react'
import { useDispatch } from 'react-redux';
import { closedDrawer, openDrawer } from '../actions/ui';
import IOpenArrowButtonIcon from '../components/types/IOpenArrowButtonIcon';

export const useOpenDrawerContent = () => { 

    const dispatch = useDispatch(); 
    const [isOpenCustomDrawerContent, setOpenCustomDrawerContent] = useState<IOpenArrowButtonIcon>({});

    const openAndCloseCustomDrawerContent = (evento: string) => {
        setOpenCustomDrawerContent({ [evento]: !isOpenCustomDrawerContent[evento] });  
    };

    const closeTheDrawer = () => {
        dispatch(closedDrawer())
    };

    const opendTheDrawer = () => {
        dispatch(openDrawer())
    };

    return{
        isOpenCustomDrawerContent,
        openAndCloseCustomDrawerContent,
        closeTheDrawer,
        opendTheDrawer
    };
};
