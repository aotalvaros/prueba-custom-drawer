import { types } from "../types/types";

const initialState = {
    active: true
};

export const uiReducer = (state = initialState, action: any) =>{

    switch (action.type) {
        case types.uiOpenDrawerComponent:
            return {
                ...state,
                active: true
            }
        case types.uiCloseDrawerComponent:
            return{
                ...state,
                active: false
            }           
        default:
            return state;
    };

};