import { types } from "../types/types";


export const openDrawer = () => ({
    type: types.uiOpenDrawerComponent   
});

export const closedDrawer = () => ({
    type: types.uiCloseDrawerComponent   
});