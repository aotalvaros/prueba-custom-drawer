
export const types = { 
    uiOpenDrawerComponent:     '[UI] Open Drawer Component',
    uiCloseDrawerComponent:    '[UI] Close Drawer Component',
};