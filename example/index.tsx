import 'react-app-polyfill/ie11';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {GobantDrawerComponent} from '../dist/components/GobantDrawerComponent';

const exampleDrawerContent = {       
  title: '¡Bienvenido!',   
  content: [
    {
      "itemTitleDrawer": "Secretar\u00cda TIC",
      "path": "/secretaria_tic",
      "subItemTitleDrawer": [
          {
              "name": "objeto1",
              "path": "/secretaria_tic/objeto1"
          },
          {
              "name": "objeto2",
              "path": "/secretaria_tic/objeto2"
          }
      ]
  },
  {
      "itemTitleDrawer": "Secretar\u00cda Seguridad",
      "path": "/secretaria_seguridad",
      "subItemTitleDrawer": [
          { 
              "name": "objeto1",
              "path": "/secretaria_seguridad/objeto1"
          
          }, 
          {
              "name": 'objeto2',
              "path": "/secretaria_seguridad/objeto2"
          },
          {
              "name": 'objeto3',
              "path": "/secretaria_seguridad/objeto3"
          },
          {
              "name": 'objeto4',
              "path": "/secretaria_seguridad/objeto4"
          }
      ],            
  },                    
  {
      "itemTitleDrawer": "Secretar\u00cda Salud",
      "path": "/secretaria_salud",
      "subItemTitleDrawer": [],
  },
  {
      "itemTitleDrawer": "Secretar\u00cda Infrastructura",
      "path": "/secretaria_infrastructura",
      "subItemTitleDrawer":[
          {
              "name": "objeto1",
              "path": "/secretaria_infrastructura/objeto1",
          },
          {
              "name": "objeto2",
              "path": "/secretaria_infrastructura/objeto2",
          },
          {
              "name": "objeto3",
              "path": "/secretaria_infrastructura/objeto3"
          }
      ]
  }
  ],
}

const App = () => {
  return (
    <GobantDrawerComponent
        title={exampleDrawerContent.title}
        content={exampleDrawerContent.content}
        showIconButtonMenu={true}
      />
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
